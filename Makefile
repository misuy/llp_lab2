.PHONY: all clean build run

CC_FLAGS = -Werror -Wall -Wextra -Wmissing-prototypes -Wstrict-prototypes
CC = gcc
LD = ld
SRC_DIR = src
BUILD_DIR = build
TO_BUILD = \
	$(BUILD_DIR)/finite_automata.o \
	$(BUILD_DIR)/lex.o \
	$(BUILD_DIR)/main.o \
	$(BUILD_DIR)/syntax.o \
	$(BUILD_DIR)/text.o \
	$(BUILD_DIR)/text.o \
	$(BUILD_DIR)/token.o \
	$(BUILD_DIR)/tree.o \
	$(BUILD_DIR)/parser.o \

TARGET = $(BUILD_DIR)/lab2



$(shell mkdir -p $(BUILD_DIR))

all: build

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CC_FLAGS) -c -o $@ $^

$(TARGET): $(TO_BUILD)
	$(CC) -lm -o $@ $^

build: $(TARGET)

run: build
	./$(TARGET)

clean:
	rm -rf $(BUILD_DIR)