#include "tree.h"

#define SPACES_PER_LEVEL 3

void print_spaces(FILE *file, uint32_t n);
char * unary_operator_get_name(UnaryOperator operator);
char * binary_operator_get_name(BinaryOperator operator);
char * type_get_name(DataType type);
char * node_get_type_name(Node *node);



void print_spaces(FILE *file, uint32_t n)
{
    for (uint32_t i=0; i<n; i++)
    {
        fprintf(file, " ");
    }
}

char * unary_operator_get_name(UnaryOperator operator)
{
    switch (operator)
    {
        case OP_NOT:
            return "!(not)";
        default:
            return "unknown";
    }
}

char * binary_operator_get_name(BinaryOperator operator)
{
    switch (operator)
    {
        case OP_AND:
            return "&(and)";
        case OP_OR:
            return "|(or)";
        case OP_EQUAL:
            return "==(equal)";
        case OP_NOT_EQUAL:
            return "!=(not equal)";
        case OP_LESS:
            return "<(less)";
        case OP_NOT_GREATER:
            return "<=(not greater)";
        case OP_GREATER:
            return ">(greater)";
        case OP_NOT_LESS:
            return ">=(not less)";
        case OP_SUBSTR:
            return "substr(substring)";
        default:
            return "unknown";
    }
}

char * type_get_name(DataType type)
{
    switch (type)
    {
        case TYPE_BOOL:
            return "bool";
        case TYPE_UINT_8:
            return "uint_8";
        case TYPE_UINT_16:
            return "uint_16";
        case TYPE_UINT_32:
            return "uint_32";
        case TYPE_INT_32:
            return "int_32";
        case TYPE_FLOAT:
            return "float";
        case TYPE_STRING:
            return "string";
        default:
            return "unknown";
    }
}

char * node_get_type_name(Node *node)
{
    if (node == 0)
        return "null";
    
    switch (node->type)
    {
        case NODE_ERROR:
            return "error";
        case NODE_BOOL_CONSTANT:
            return "bool_constant";
        case NODE_INTEGER_CONSTANT:
            return "integer_constant";
        case NODE_FLOAT_CONSTANT:
            return "float_constant";
        case NODE_STRING_CONSTANT:
            return "string_constant";
        case NODE_CONSTANTS_LIST:
            return "constants_list";
        case NODE_TYPE:
            return "type";
        case NODE_TYPES_LIST:
            return "types_list";
        case NODE_VARIABLE:
            return "variable";
        case NODE_COLUMN_PTR:
            return "column_ptr";
        case NODE_PROJECTION:
            return "projection";
        case NODE_UNARY_EXPRESSION:
            return "unary_expression";
        case NODE_BINARY_EXPRESSION:
            return "binary_expression";
        case NODE_TABLE_DATA_SOURCE:
            return "table_data_source";
        case NODE_JOIN_DATA_SOURCE:
            return "join_data_source";
        case NODE_FILTER:
            return "filter";
        case NODE_FILTER_EMPTY:
            return "filter_empty";
        case NODE_ASSIGNMENT:
            return "assignment";
        case NODE_ASSIGNMENTS_LIST:
            return "assignments_list";
        case NODE_DATA_TUPLES_LIST:
            return "data_tuples_list";
        case NODE_SELECT_QUERY:
            return "select_query";
        case NODE_DELETE_QUERY:
            return "delete_query";
        case NODE_INSERT_QUERY:
            return "insert_query";
        case NODE_UPDATE_QUERY:
            return "update_query";
        case NODE_CREATE_TABLE_QUERY:
            return "create_table_query";
        case NODE_DELETE_TABLE_QUERY:
            return "delete_table_query";
        default:
            return "unknown";
    }
}

void node_print(FILE *file, Node *node, char *name, uint32_t level)
{
    if (node == 0)
    {
        print_spaces(file, (level - 1) * SPACES_PER_LEVEL);
        fprintf(file, "%s: null,\n", name);
        return;
    }
    
    print_spaces(file, (level - 1) * SPACES_PER_LEVEL);
    fprintf(file, "%s: {\n", name);
    print_spaces(file, level * SPACES_PER_LEVEL);
    //printf("%d\n", node->type);
    fprintf(file, "node_type: %s,\n", node_get_type_name(node));

    switch (node->type)
    {
        case NODE_ERROR:
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "error_offset: %"PRIu32"\n", node->error.error_offset);
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "error_message:\n%s,\n", node->error.error_message);
            node_print(file, node->error.tree, "tree", level + 1);
            break;

        case NODE_BOOL_CONSTANT:
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "value: \'%s\',\n", node->bool_contant.value ? "t" : "f");
            break;

        case NODE_INTEGER_CONSTANT:
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "value: %"PRIu32",\n", node->integer_constant.value);
            break;

        case NODE_FLOAT_CONSTANT:
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "value: %f,\n", node->float_contant.value);
            break;

        case NODE_STRING_CONSTANT:
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "value: \'%s\',\n", node->string_constant.value);
            break;

        case NODE_CONSTANTS_LIST:
            node_print(file, node->constants_list.constant, "constant", level + 1);
            node_print(file, node->constants_list.next, "next", level + 1);
            break;

        case NODE_TYPE:
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "type: %s,\n", type_get_name(node->typee.type));
            break;
        
        case NODE_TYPES_LIST:
            node_print(file, node->types_list.type, "type", level + 1);
            node_print(file, node->types_list.next, "next", level + 1);
            break;

        case NODE_VARIABLE:
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "name: \'%s\',\n", node->variable.name);
            break;

        case NODE_COLUMN_PTR:
            node_print(file, node->column_ptr.table_name, "table_name", level + 1);
            node_print(file, node->column_ptr.column_name, "column_name", level + 1);
            break;

        case NODE_PROJECTION:
            node_print(file, node->projection.column_ptr, "column_ptr", level + 1);
            node_print(file, node->projection.next, "next", level + 1);
            break;
        
        case NODE_UNARY_EXPRESSION:
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "operator: %s,\n", unary_operator_get_name(node->unary_expression.operator));
            node_print(file, node->unary_expression.operand, "operand", level + 1);
            break;
        
        case NODE_BINARY_EXPRESSION:
            print_spaces(file, level * SPACES_PER_LEVEL);
            fprintf(file, "operator: %s,\n", binary_operator_get_name(node->binary_expression.operator));
            node_print(file, node->binary_expression.left_operand, "left_operand", level + 1);
            node_print(file, node->binary_expression.right_operand, "right_operand", level + 1);
            break;
        
        case NODE_TABLE_DATA_SOURCE:
            node_print(file, node->table_data_source.table, "table", level + 1);
            node_print(file, node->table_data_source.variable, "variable", level + 1);
            break;
        
        case NODE_JOIN_DATA_SOURCE:
            node_print(file, node->join_data_source.expression, "expression", level + 1);
            node_print(file, node->join_data_source.left_data_source, "left_data_source", level + 1);
            node_print(file, node->join_data_source.right_data_source, "right_data_source", level + 1);
            break;

        case NODE_FILTER:
            node_print(file, node->filter.expression, "expression", level + 1);
            break;
        
        case NODE_FILTER_EMPTY:
            break;

        case NODE_ASSIGNMENT:
            node_print(file, node->assignment.to, "to", level + 1);
            node_print(file, node->assignment.what, "what", level + 1);
            break;
        
        case NODE_ASSIGNMENTS_LIST:
            node_print(file, node->assignments_list.assignment, "assignment", level + 1);
            node_print(file, node->assignments_list.next, "next", level + 1);
            break;

        case NODE_DATA_TUPLES_LIST:
            node_print(file, node->data_tuples_list.data_tuple, "data_tuple", level + 1);
            node_print(file, node->data_tuples_list.next, "next", level + 1);
            break;
        
        case NODE_SELECT_QUERY:
            node_print(file, node->select_query.data_source, "data_source", level + 1);
            node_print(file, node->select_query.filter, "filter", level + 1);
            node_print(file, node->select_query.projection, "projection", level + 1);
            break;

        case NODE_DELETE_QUERY:
            node_print(file, node->delete_query.data_source, "data_source", level + 1);
            node_print(file, node->delete_query.filter, "filter", level + 1);
            break;

        case NODE_INSERT_QUERY:
            node_print(file, node->insert_query.table_name, "table_name", level + 1);
            node_print(file, node->insert_query.data_tuples, "data_tuples", level + 1);
            break;

        case NODE_UPDATE_QUERY:
            node_print(file, node->update_query.data_source, "data_source", level + 1);
            node_print(file, node->update_query.filter, "filter", level + 1);
            node_print(file, node->update_query.assignments, "assignments", level + 1);
            break;
        
        case NODE_CREATE_TABLE_QUERY:
            node_print(file, node->create_table_query.table_name, "table_name", level + 1);
            node_print(file, node->create_table_query.types, "types", level + 1);
            break;
        
        case NODE_DELETE_TABLE_QUERY:
            node_print(file, node->delete_table_query.table_name, "table_name", level + 1);
            break;

        default:
            break;
    }

    print_spaces(file, (level - 1) * 3);
    fprintf(file, "},\n");
}

void node_free(Node *node)
{
    if (node == 0)
        return;
        
    switch (node->type)
    {
        case NODE_ERROR:
            free(node->error.error_message);
            node_free(node->error.tree);
            break;

        case NODE_BOOL_CONSTANT:
        case NODE_INTEGER_CONSTANT:
        case NODE_FLOAT_CONSTANT:
            break;
        case NODE_STRING_CONSTANT:
            free(node->string_constant.value);
            break;
        case NODE_CONSTANTS_LIST:
            node_free(node->constants_list.constant);
            node_free(node->constants_list.next);
            break;

        case NODE_TYPE:
            break;
        case NODE_TYPES_LIST:
            node_free(node->types_list.type);
            node_free(node->types_list.next);
            break;

        case NODE_VARIABLE:
            free(node->variable.name);
            break;
        
        case NODE_COLUMN_PTR:
            node_free(node->column_ptr.table_name);
            node_free(node->column_ptr.column_name);
            break;
        
        case NODE_PROJECTION:
            node_free(node->projection.column_ptr);
            node_free(node->projection.next);
            break;

        case NODE_UNARY_EXPRESSION:
            node_free(node->unary_expression.operand);
            break;
        case NODE_BINARY_EXPRESSION:
            node_free(node->binary_expression.left_operand);
            node_free(node->binary_expression.right_operand);
            break;

        case NODE_TABLE_DATA_SOURCE:
            node_free(node->table_data_source.table);
            node_free(node->table_data_source.variable);
            break;
        case NODE_JOIN_DATA_SOURCE:
            node_free(node->join_data_source.left_data_source);
            node_free(node->join_data_source.right_data_source);
            node_free(node->join_data_source.expression);
            break;
        
        case NODE_FILTER:
            node_free(node->filter.expression);
            break;
        case NODE_FILTER_EMPTY:
            break;

        case NODE_ASSIGNMENT:
            node_free(node->assignment.to);
            node_free(node->assignment.what);
            break;
        case NODE_ASSIGNMENTS_LIST:
            node_free(node->assignments_list.assignment);
            node_free(node->assignments_list.next);
            break;
        
        case NODE_DATA_TUPLES_LIST:
            node_free(node->data_tuples_list.data_tuple);
            node_free(node->data_tuples_list.next);
            break;

        case NODE_SELECT_QUERY:
            node_free(node->select_query.data_source);
            node_free(node->select_query.filter);
            node_free(node->select_query.projection);
            break;

        case NODE_DELETE_QUERY:
            node_free(node->delete_query.data_source);
            node_free(node->delete_query.filter);
            break;

        case NODE_INSERT_QUERY:
            node_free(node->insert_query.data_tuples);
            node_free(node->insert_query.table_name);
            break;
        
        case NODE_UPDATE_QUERY:
            node_free(node->update_query.assignments);
            node_free(node->update_query.data_source);
            node_free(node->update_query.filter);
            break;
        
        case NODE_CREATE_TABLE_QUERY:
            node_free(node->create_table_query.table_name);
            node_free(node->create_table_query.types);
            break;

        case NODE_DELETE_TABLE_QUERY:
            node_free(node->delete_table_query.table_name);
            break;

        default:
            break;
    }
    free(node);
}