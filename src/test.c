#include "parser.h"

#define BUFFER_SIZE 1000

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        printf("usage: ");
    }
    FiniteAutomata *automata = finite_automata_new(tokens_default(), DEFAULT_TOKENS_COUNT, finite_automata_alphabet_default());

    char buffer[BUFFER_SIZE];
    uint32_t offset = 0;
    while ((buffer[offset] = getchar()) != '\n')
    {
        offset++;
    }
    buffer[offset] = 0;

    Text *text = text_new(buffer);

    Node* node;

    yypstate *parser = yypstate_new();

    int res = yypull_parse(parser, automata, text, &node);

    if (res)    
        node_print(stdout, node, "error", 1);
    else
        node_print(stdout, node, "query", 1);

    node_free(node);

    return 0;
}